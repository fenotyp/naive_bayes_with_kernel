# Example of Naive Bayes implemented from Scratch in Python
import random
import pandas as pd
import numpy as np
import math
import matplotlib.pyplot as plt
from sklearn.neighbors import KernelDensity

class sumarize():
    fig = 0
    def __init__(self,observations=[], bins=0):
        sumarize.fig += 1
        self.bins = bins
        self._observations = np.array(observations)
        # drop zeros
        if all(i==self._observations[0] for i in self._observations):
            pass
        else:
            #    self._observations = [i for i in self._observations if i != 0]
            dist = max(self._observations) - min(self._observations)
            x_d = np.linspace(min(self._observations)-dist/3, max(self._observations)+dist/3, 200)
        ##    self._Y, self._X = np.histogram(self._observations, bins='auto', density=True)
        ##    if self._X[0] == 0 and self._Y[0] > self._Y[1]:
        ##        self._observations = np.array([i for i in self._observations if i != 0])
            ##    self._Y, self._X = np.histogram(self._observations, bins='auto', density=True)

            # instantiate and fit the KDE model

            kde = KernelDensity(bandwidth=dist/19.0, kernel='gaussian')

            kde.fit(self._observations[:, None])

            # score_samples returns the log of the probability density
            logprob = kde.score_samples(x_d[:, None])
            '''
            plt.fill_between(x_d, np.exp(logprob), alpha=0.5)
            plt.plot(self._observations, np.full_like(self._observations, -0.01), '|k', markeredgewidth=1)
            #plt.ylim(-0.02, 0.22)

            #print(self._X)
            #plt.figure(sumarize.fig)
            plt.hist(self._observations, bins='auto', alpha=0.5, color='red', normed='True')  # arguments are passed to np.histogram
            #plt.title("Histogram with 'auto' bins")
            plt.show()
            #self._Y, self._X = np.histogram(self._observations, bins=self.bins, density=True)
            '''
            self._Y = np.exp(logprob)
            self._X = x_d

    def probability(self, x):
        for i in range(len(self._X)-1):

            if x >= self._X[i] and x <= self._X[i+1]:
                return self._Y[i]

        return 0


def splitDataset(dataset, splitRatio):

    trainSize = int(len(dataset) * splitRatio)
    trainSet = []
    copy = list(dataset)
    while len(trainSet) < trainSize:
        index = random.randrange(len(copy))
        trainSet.append(copy.pop(index))
    return [trainSet, copy]


def separateByClass(dataset):

    separated = {}
    for i in range(len(dataset)):
        vector = dataset[i]
        if (vector[-1] not in separated):
            separated[vector[-1]] = []
        separated[vector[-1]].append(vector)
    return separated


def summarize(dataset):

    summaries = [(np.mean(attribute), np.std(attribute)) for attribute in zip(*dataset)]
    del summaries[-1]
    return summaries

def PS_summarize(dataset):
    """
    Create lists of instances containing probability distribution
    :param dataset: list of data set from observations
    :return: list of instance sumarize containing probability distribution
    """

    summaries = [sumarize(attribute, bins=50) for attribute in zip(*dataset)]
    del summaries[-1]
    return summaries


def summarizeByClass(dataset):

    separated = separateByClass(dataset)
    summaries = {}
    for classValue, instances in separated.items():
        summaries[classValue] = summarize(instances)
    return summaries

def PS_summarizeByClass(dataset):
    """
    Create probability distribution by Class
    :param dataset:
    :return:
    """
    separated = separateByClass(dataset)
    summaries = {}
    for classValue, instances in separated.items():
        summaries[classValue] = PS_summarize(instances)
    return summaries


def calculateProbability(x, mean, stdev):
    """
    Should be corrected to calculate prob from real distribution based on obserwations
    :param x:
    :param mean:
    :param stdev:
    :return:
    """
    exponent = math.exp(-(math.pow(x - mean, 2) / (2 * math.pow(stdev, 2))))
    return (1 / (math.sqrt(2 * math.pi) * stdev)) * exponent


def calculateClassProbabilities(summaries, inputVector):
    probabilities = {}
    for classValue, classSummaries in summaries.items():
        probabilities[classValue] = 1
        for i in range(len(classSummaries)):
            mean, stdev = classSummaries[i]
            x = inputVector[i]
            probabilities[classValue] *= calculateProbability(x, mean, stdev)
    return probabilities

def PS_calculateClassProbabilities(summaries, inputVector):
    """

    :param summaries:
    :param inputVector:
    :return:
    """
    probabilities = {}
    for classValue, classSummaries in summaries.items():
        probabilities[classValue] = 1
        for i in range(len(classSummaries)):
            x = inputVector[i]
            #print(classSummaries)
            probabilities[classValue] *= classSummaries[i].probability(x)
    #print("return, ", probabilities)
    return probabilities


def predict(summaries, inputVector):
    probabilities = calculateClassProbabilities(summaries, inputVector)
    bestLabel, bestProb = None, -1
    for classValue, probability in probabilities.items():
        if bestLabel is None or probability > bestProb:
            bestProb = probability
            bestLabel = classValue
    return bestLabel

def PS_predict(summaries, inputVector):
    """

    :param summaries:
    :param inputVector:
    :return:
    """
    probabilities = PS_calculateClassProbabilities(summaries, inputVector)
    bestLabel, bestProb = None, -1
    for classValue, probability in probabilities.items():
        if bestLabel is None or probability > bestProb:
            bestProb = probability
            bestLabel = classValue
    return bestLabel


def getPredictions(summaries, testSet):
    predictions = []
    for i in range(len(testSet)):
        result = predict(summaries, testSet[i])
        predictions.append(result)
    return predictions

def PS_getPredictions(summaries, testSet):
    """

    :param summaries:
    :param testSet:
    :return:
    """
    predictions = []
    for i in range(len(testSet)):
        result = PS_predict(summaries, testSet[i])
        predictions.append(result)
    return predictions


def getAccuracy(testSet, predictions):
    correct = 0
    for i in range(len(testSet)):
        if testSet[i][-1] == predictions[i]:
            correct += 1
    return (correct / float(len(testSet))) * 100.0




if __name__ == "__main__":

    filename = "pima-indians-diabetes.data.csv"
    df = pd.read_csv(filename, sep=",", header=None)
    REF = np.zeros(91-30)
    PRO = np.zeros(91-30)
    ile = 50
    for repet in range(ile):
        for i in range(30, 91):
        #for i in range(90, 91):
            splitRatio = i/100.0
            #print(splitRatio, end=' ')

            dataset = df.values
            trainingSet, testSet = splitDataset(dataset, splitRatio)
            #print("Split {0} rows into train={1} and test={2} rows".format(len(dataset), len(trainingSet), len(testSet)))
            # prepare model
            summaries = summarizeByClass(trainingSet)
            PS_summaries = PS_summarizeByClass(trainingSet)
            #print(summaries)
            #print(PS_summaries)
            # test model
            predictions = getPredictions(summaries, testSet)
            PS_predictions = PS_getPredictions(PS_summaries, testSet)

            #plt.show()
            accuracy = getAccuracy(testSet, predictions)
            PS_accuracy = getAccuracy(testSet, PS_predictions)
            REF[i-30] += accuracy/ile
            PRO[i-30] += PS_accuracy/ile
            #print("Accuracy oryginal: {0}%\tAccuracy from probability_distribution: {1}%".format(accuracy,PS_accuracy))
    for j in range(30,91):
        print("Accuracy after {0} {1} {2}% {3}%".format(ile,j/100,REF[j-30],PRO[j-30]))
